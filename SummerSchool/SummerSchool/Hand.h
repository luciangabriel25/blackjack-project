#pragma once
class Card;

class Hand
{
public:
	Hand();
	~Hand();

	void AddCard(Card* card);
	size_t GetValue() const;

	void Print();

private:
	size_t m_numCards;
	Card** m_cards;
};

