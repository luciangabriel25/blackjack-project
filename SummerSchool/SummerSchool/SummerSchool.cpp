// SummerSchool.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Player.h"
#include "Game.h"
#include "Configuration.h"
#include <fstream>
#include <iostream>
#include "SFML/Graphics.hpp"
#include <unordered_map>
#include <filesystem>
#include <sstream>
#include "Any.h"
#include "TestData.h"

static std::vector<sf::Texture*> textures;

void loadSprites(std::unordered_map<int, sf::Sprite*>& sprites)
{
	static int imageNumber = 102;
	int counter = 0;
	//std::string path = R"(images\103.gif
	std::string path = "images\\";
	std::stringstream ss;
	for (int index = 0; index < 52; ++index)
	{
		textures.push_back(new sf::Texture());
		ss << path << imageNumber << ".gif";
		std::string texturePath;
		ss >> texturePath;
		textures[textures.size() -1]->loadFromFile(texturePath);

		std::cout << "Texture" << imageNumber << " Loaded !" << std::endl;

		sprites[imageNumber] = new sf::Sprite();
		sprites[imageNumber]->setTexture(*textures[textures.size() - 1]);

		++imageNumber;
		++counter;
		if (counter == 13)
		{
			imageNumber += 100-13;
			counter = 0;
		}
		ss.clear();
	}
}

int main()
{
	Player* players[] =
	{
		new Player("Player 1"),
		new Player("Player 2")
	};
	size_t numPlayers = sizeof(players) / sizeof(Player*);

	Game game(numPlayers, players);

	game.Play();

	for (Player* player : players)
	{
		delete player;
		player = nullptr;
	}
	
	std::string path = R"(C:\Users\xxu9oc\Downloads\Blackjack\SummerSchool\SummerSchool\config.ini)";
	try
	{
		Configuration* config = new Configuration(path);
		config->printConfiguration();
	}
	catch (ConfigurationError ErrorType)
	{
		switch (ErrorType)
		{
		case ConfigurationError::CantOpenFile:
		{
			std::cerr << "Can't open file !" << std::endl;
			break;
		}
		case ConfigurationError::EmptyFile:
		{
			std::cerr << "The configuration file is empty !" << std::endl;
			break;
		}
		case ConfigurationError::MalformedEntry:
		{
			std::cerr << "The configuration is malformed !" << std::endl;
			break;
		}
		default:
			break;
		}
	}


	/*
	int windowWidth = 1000;
	int windowHeight = 500;
	sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "BlackJack");
	window.setFramerateLimit(60);
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);
	
	std::unordered_map<int, sf::Sprite*> sprites;

	loadSprites(sprites);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::White);

		window.draw(shape);
		for (int i = 102; i < 114; ++i)
		{
			window.draw(*sprites[i]);
		}
		

		window.display();
	}
    */

	//{
	//	// Test 1
	//	Any a;
	//	int val = 2;
	//	a = val;
	//	std::cout << "Test with int "
	//		<< ((a.get<int>() == val) ? "PASSED" : "FAILED") << std::endl;
	//}

	//{
	//	// Test 2
	//	Any a;
	//	int val = 2;
	//	a = val;
	//	val = 7;
	//	a = val;
	//	std::cout << "Test with 2 ints "
	//		<< ((a.get<int>() == val) ? "PASSED" : "FAILED") << std::endl;
	//}

	//{
	//	// Test 3
	//	Any a;
	//	double val = 2.314;
	//	a = val;
	//	std::cout << "Test with double "
	//		<< ((a.get<double>() == val) ? "PASSED" : "FAILED") << std::endl;
	//}

	//{
	//	// Test 4
	//	Any a;
	//	TestData t;
	//	a = t; // abort
	//}

	//{
	//	// Test 5
	//	Any a;
	//	double val = 2.314;
	//	a = val;
	//	{
	//		Any b(a);
	//	}
	//	std::cout << "Rule of 3 (five) test " << ((a.get<double>() == val) ? "PASSED" : "FAILED") << std::endl;
	//}

	//{
	//	// Test 6
	//	Any a;
	//	double val = 2.314;
	//	double val2 = 22.3;
	//	a = val;
	//	a = val2;
	//	
	//	std::cout << "Two doubles same Any test " << ((a.get<double>() == val) ? "PASSED" : "FAILED") << std::endl;
	//}







	return 0;
}

