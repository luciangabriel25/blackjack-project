#pragma once
#include <functional>

typedef void(*deleterFunction)(void*);
typedef void(*copierFunction)(void*, void*);

template<typename T>
void templateDeleter(void* p)
{
	T* asT = static_cast<T*>(p);
	delete asT;
}

template<typename T>
void copyFunction(void* dest, void* src)
{
	T* destAsT = (T*)dest;
	T* srcAsT = (T*)src;
	*destAsT = *srcAsT;
}

class Any
{
public:
	using func = std::function<void()>;
	Any();
	~Any();
	template<typename T>
	Any& operator=(const T other);
	Any(const Any& other);
	template<typename T>
	T get();
private:
	void* m_addressOfValue;
	//deleterFunction m_deleter;
	std::function<void(void*)> m_deleter;
	copierFunction m_copier;
};

template<typename T>
inline Any& Any::operator=(const T other)
{
	this->~Any();
	//m_deleter = templateDeleter<T>;
	m_deleter = [](void* p) {
		T* cast = static_cast<T*>(p);
		delete cast;
	};
	m_copier = copyFunction<T>;
	m_addressOfValue = new T(other);
	return *this;
}

template<typename T>
inline T Any::get()
{
	T* tval = static_cast<T*>(m_addressOfValue);
	return *tval;
}
