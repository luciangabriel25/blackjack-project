#include "stdafx.h"
#include "Configuration.h"
#include <algorithm>
#include <iostream>

Configuration::Configuration(std::string path)
	:m_good(false), m_lastPath(path)
{
	Load();
}

Configuration::~Configuration()
{

}

void Configuration::Load()
{
	input.open(m_lastPath);
	if (input.good())
	{
		std::string line;
		size_t linesCount = 0;
		while (getline(input, line)) 
		{
			++linesCount;
			m_good = true;
			line.erase(std::remove_if(line.begin(), line.end(), isspace),line.end());
			if (line[0] == '#' || line.empty())
			{
				continue;
			}
			auto delimiterPos = line.find("=");
			auto name = line.substr(0, delimiterPos);
			auto value = line.substr(delimiterPos + 1);
			int val;
			try
			{
				val = std::stoi(value);
			}
			catch (...)
			{
				throw ConfigurationError::MalformedEntry;
			}
			settings[name] = val;
		}
		if (linesCount == 0)
		{
			throw ConfigurationError::EmptyFile;
		}
	}
	else
	{
		throw ConfigurationError::CantOpenFile;
	}
}

int Configuration::operator[](std::string setting)
{
	return settings[setting];
}

void Configuration::printConfiguration() const noexcept
{
	for (auto it = settings.begin(); it != settings.end(); ++it)
	{
		std::cout << it->first << " " << it->second << std::endl;
	}
}

bool Configuration::good() const
{
	return m_good;
}

void Configuration::reload()
{
	Load();
}