#include <iostream>
#pragma once


class TestData
{
public:
	TestData()
	{
		m_array = new int[512];
	}
	~TestData()
	{
		delete[] m_array;
		std::cout << "Deleted object " << this << std::endl;
	}
	TestData(const TestData& other)
	{
		m_array = new int[512];
		for (int i = 0; i < 512; ++i)
			m_array[i] = other.m_array[i];
	}
private:
	int* m_array;
};